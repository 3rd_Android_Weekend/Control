package com.kshrd.control;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.kshrd.control.entity.Country;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpinnerActivity extends AppCompatActivity {

    @BindView(R.id.spinner)
    Spinner spinner;

    private List<Country> countryList;

    private int check = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        ButterKnife.bind(this);

        //final String[] brands = {"Honda", "Suzuki", "Yamaha", "--Select--"};

        countryList = new ArrayList<>();
        countryList.add(new Country(1, "KH"));
        countryList.add(new Country(2, "US"));
        countryList.add(new Country(3, "VN"));
        countryList.add(new Country(4, "TH"));
        countryList.add(new Country(0, "--Select--"));

        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(
                this,
                android.R.layout.simple_list_item_1,
                countryList
        ) {
            @Override
            public int getCount() {
                int count = super.getCount();
                return (count > 0) ? (count - 1) : count;
            }
        };

        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (++check > 1){
                    Country country = (Country) parent.getSelectedItem();
                    int countryId = country.getId();
                    Toast.makeText(SpinnerActivity.this, String.valueOf(countryId), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}

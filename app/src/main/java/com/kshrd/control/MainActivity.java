package com.kshrd.control;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.button);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "Annonymous Class", Toast.LENGTH_SHORT).show();
//            }
//        });

        btn.setOnClickListener(this);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Annoymous Class outside the method", Toast.LENGTH_SHORT).show();
            }
        };
        findViewById(R.id.button3).setOnClickListener(onClickListener);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.button:
                Toast.makeText(this, "Implement Interface", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    public void buttonClick(View view) {
        Toast.makeText(this, "Button Click by Attribute", Toast.LENGTH_SHORT).show();
    }
}

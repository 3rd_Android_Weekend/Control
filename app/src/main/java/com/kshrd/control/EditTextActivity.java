package com.kshrd.control;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditTextActivity extends AppCompatActivity implements TextWatcher {

    @BindView(R.id.etKeyword)
    EditText etKeyword;

    Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);
        ButterKnife.bind(this);

        handler = new Handler();
        etKeyword.addTextChangedListener(this);

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.e("ooooo", etKeyword.getText().toString());
            }
        };
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000);

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

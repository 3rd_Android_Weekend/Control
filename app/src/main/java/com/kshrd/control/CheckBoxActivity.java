package com.kshrd.control;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckBoxActivity extends AppCompatActivity implements
        CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.cbPhp)
    CheckBox cbPhp;

    @BindView(R.id.cbVb)
    CheckBox cbVb;

    @BindView(R.id.cbJava)
    CheckBox cbJava;

    @BindView(R.id.cbRuby)
    CheckBox cbRuby;

    @BindView(R.id.tvSubject)
    TextView tvSubject;

    private List<String> subjectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);
        ButterKnife.bind(this);

        // Initialize
        subjectList = new ArrayList<>();

        cbPhp.setOnCheckedChangeListener(this);
        cbVb.setOnCheckedChangeListener(this);
        cbJava.setOnCheckedChangeListener(this);
        cbRuby.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton checkbox, boolean isChecked) {

        String value = checkbox.getText().toString();
        if (isChecked) {
            subjectList.add(value);
        } else {
            subjectList.remove(value);
        }



        /* ------------------------------------------------------------ */
        // For Different Operation Purpose
        /*switch (checkbox.getId()){
            case R.id.cbPhp:
                if (isChecked){
                    Toast.makeText(this, "1st Operation Checked", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "1st Operation UnChecked", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.cbVb:
                Toast.makeText(this, "2st Operation", Toast.LENGTH_SHORT).show();
                break;
            case R.id.cbJava:
                Toast.makeText(this, "3rd Operation", Toast.LENGTH_SHORT).show();
                break;
            case R.id.cbRuby:
                Toast.makeText(this, "4th Operation", Toast.LENGTH_SHORT).show();
                break;
        }*/
    }


    @OnClick(R.id.btnSubmit)
    void onSubmit(){
        String str = "";

        if (!subjectList.isEmpty()){
            str += "Selected Subject : \n";
            for (String subject : subjectList) {
                str += subject + "\n";
            }
        } else {
            str += "No subject selected!";
        }

        tvSubject.setText(str);
    }
}

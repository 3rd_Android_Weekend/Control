package com.kshrd.control;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RadioButtonActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.rgGender)
    RadioGroup rgGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);

        rgGender.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

        switch (group.getId()) {
            case R.id.rgGender:
                // All radio buttons have same operation
//                RadioButton rb = (RadioButton) findViewById(checkedId);
//                String value = rb.getText().toString();
//                Toast.makeText(this, value, Toast.LENGTH_SHORT).show();

                // All radio buttons have different operation
                RadioButton rb = (RadioButton) findViewById(checkedId);
                if (rb.getId() == R.id.rbMale) {
                    Toast.makeText(this, "1st Operation", Toast.LENGTH_SHORT).show();
                } else if (rb.getId() == R.id.rbFemale) {
                    Toast.makeText(this, "2nd Operation", Toast.LENGTH_SHORT).show();
                }

                break;
        }

        RadioButton rb = (RadioButton) findViewById(checkedId);
        String value = rb.getText().toString();

        Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
    }
}
